import operator

from json import dumps

from . import log


values = operator.itemgetter('id', 'description')


class PullRequest(object):
    url = \
        'https://api.bitbucket.org/2.0/repositories/' \
        '{destination_repo}/pullrequests'

    @classmethod
    def create(
        cls,
        api,
        destination_repo,
        title,
        source_branch,
        source_repo=None,
        destination_branch=None,
        description=None,
        close_source_branch=True
    ):
        pr = {
            'close_source_branch': close_source_branch,
            'title': title,
            'source': {'branch': {'name': source_branch}},
            'destination': {
                'repository': {
                    'full_name': destination_repo
                }
            }
        }
        if source_repo:
            pr['source']['repository'] = {
                'full_name': source_repo
            }
        if destination_branch:
            pr['destination']['branch'] = {
                'name': destination_branch
            }
        if description:
            pr['description'] = description

        log.info("PullRequest.create for {}".format(pr))

        return cls(api, pr)

    def __init__(self, api, pr):
        if not pr.get('links') or not pr['links'].get('merge'):
            log.info("PullRequest init api calling for {}".format(pr))
            destination_repo = pr['destination']['repository']['full_name']
            url = PullRequest.url.format(destination_repo=destination_repo)
            request = api(url, data=dumps(pr))
        else:
            request = pr
        log.info("PullRequest init for {}".format(request))
        if request.get('error'):
            raise ValueError(request['error']['message'])
        self.request = request
        self.api = api

    def __str__(self):
        return str(values(self.__dict__))

    def __repr__(self):
        repr = "".join((
            "{name}.create(",
            "{api}, '{destination_repo}', '{title}', '{source_branch}'",
            ")",
        ))
        name = self.__class__.__name__
        api = self.api
        destination_repo = self.request[
            'destination'][
            'repository'][
            'full_name'
        ]
        title = self.request['title']
        source_branch = self.request['source']['branch']['name']
        return repr.format(**locals())

    def merge(self, message="automated merge"):
        log.info("Merging {} with message '{}'".format(self.request, message))
        url = self.request['links']['merge']['href']
        data = {'message': message}
        request = self.api(url, data=dumps(data))
        if request.get('error'):
            raise ValueError(request['error']['message'])
        self.request = request

    def decline(self, message="this pull request has failed to merge"):
        url = self.request['links']['decline']['href']
        data = {'message': message}
        request = self.api(url, data=dumps(data))
        if request.get('error'):
            raise ValueError(request['error']['message'])
        self.request = request
