from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session


TOKEN_URL = 'https://bitbucket.org/site/oauth2/access_token'


class API(object):

    def __token_saver(self, session_token):
        self.token = session_token

    def __init__(
        self,
        client_id,
        client_secret,
        auto_refresh_kwargs={},
        headers={
            'User-Agent': 'HesburghLibrary/0.1 by ssummer3',
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
    ):
        client = BackendApplicationClient(client_id=client_id)
        oauth = OAuth2Session(client=client)
        token = oauth.fetch_token(
            token_url=TOKEN_URL,
            client_id=client_id,
            client_secret=client_secret
        )
        self.session = OAuth2Session(
            client_id,
            token=token,
            auto_refresh_url=TOKEN_URL,
            auto_refresh_kwargs=auto_refresh_kwargs,
            token_updater=self.__token_saver
        )
        self.headers = headers
        self.client_id = client_id

    def __call__(self, url, data={}, headers={}):
        headers.update(self.headers)
        if data:
            response = self.session.post(
                url,
                headers=headers,
                data=data
            )
        else:
            response = self.session.get(url, headers=headers)
        try:
            response = response.json()
        except ValueError:
            raise ValueError(': '.join((url, response.text)))
        return response.get('values', response)

    def __repr__(self):
        repr = "{name}('{client_id}', ' ***** ')"
        name = self.__class__.__name__
        return repr.format(name=name, **self.__dict__)
