import operator


class Repo(object):
    base_url = 'https://api.bitbucket.org/2.0/repositories'
    methods = {
        'pullrequests': {
            # MERGED, SUPERSEDED, OPEN, DECLINED
            'url': 'pullrequests?state={merge_state}',
            'key': operator.itemgetter('created_on'),
        },
        'commits': {
            'url': 'commits/{branchortag}',
            'key': operator.itemgetter('date'),
        },
        'activity': {
            'url': 'pullrequests/{pull_request_id}/activity',
            'key': None,
        },
    }

    def __init__(self, api, owner, repo_slug):
        self.api = api
        self.repo_full_name = '/'.join((owner, repo_slug))
        self.url_base = '/'.join((
            Repo.base_url,
            self.repo_full_name
        ))

    def __getattr__(self, attr):
        try:
            cfg = Repo.methods[attr]
        except KeyError:
            raise ValueError(
                "Not a valid method, must be one of: {}".format(
                    Repo.methods.keys()
                )
            )
        key = cfg['key']
        url = '/'.join((self.__dict__['url_base'], cfg['url']))

        def method(**kwargs_):
            try:
                uri = url.format(**kwargs_)
            except KeyError as e:
                raise ValueError(
                    "Missing required parameter: {}".format(e)
                )
            response = self.api(uri)
            result = (__ for __ in sorted(response, key=key))
            return tuple(result)
        return method

    def __repr__(self):
        repr = "{name}({api}, '{owner}', '{repo_slug}')"
        name = self.__class__.__name__
        api = self.api
        owner, repo_slug = self.repo_full_name.split('/')
        return repr.format(**locals())
