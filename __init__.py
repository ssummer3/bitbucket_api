import logging
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)
log = logging.getLogger()
'''
requests_log = logging.getLogger('requests.packages.urllib3')
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True
import httplib as http_client
http_client.HTTPConnection.debuglevel = 1
'''

from .api import API
from .repo import Repo
from .pullrequest import PullRequest

__all__ = ['API', 'Repo', 'PullRequest']
